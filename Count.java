//******************************************************************
// Honor Code: The work we are submitting is a result of our own thinking and efforts.
// Meredith Dreistadt & Lisa Taapken
// CMPSC 111 Spring 2017
// Final Project
// Date: May 1, 2017
//
// Purpose: To learn as much as possible about the attached txt file (The Signers of the Declaration of Independence).
//******************************************************************
public class Count {

	public int Count(String word1, String word2)
	{
		int val =0; //boolean phrase
		String file = word1; //Creates word1
		String word = word2; //Creates word2
		while(file.hasNextLine()){ //reads each line in txt file
			String line = file.nextLine(); //User input
			if(line.indexOf(word) != -1) //Indexing the words in the file
			{
				System.out.println("Word EXISTS in the file"); //Output if word exists
				val ++; 
				continue;
			}
			else
			{
				val = val;
				continue;
			}
		}		            		        

		return val; //Returns number of characters in the user input String
	}


}


