//******************************************************************
// Honor Code: The work we are submitting is a result of our own thinking and efforts.
// Meredith Dreistadt & Lisa Taapken
// CMPSC 111 Spring 2017
// Final Project
// Date: May 1, 2017
//
// Purpose: To learn as much as possible about the attached txt file (The Signers of the Declaration of Independence).
//******************************************************************
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Date;
import java.util.Random;

public class ProjectMain {

	public static void main(String[] args) throws FileNotFoundException {
		Random rand = new Random();
		int l = rand.nextInt(20);			       	

		System.out.println("Meredith Dreistadt and Lisa Taapken\nFinal Project\n" + new Date() + "\n");
		System.out.println("Welcome to the Natural Language Processing System! We help you find what you need.");
		Scanner input = new Scanner(System.in); //Activates scanner
		System.out.println("What would you like to do? (Search, Count, Length, Game, Scramble, Quit)");//Prompts user for initial input
		while (input.hasNext()) { //Collects user input into while loop to proceed with program as user intends
			String command = input.nextLine(); 

			String word = "";
			while(!word.matches("Quit")) //Quits the program
			{ 
				break;


			}

			while(command.equals("Search"))  //Goes through text document looking for designated word/name that user will later input      
			{
				int val=0; //Sets up boolean phrase of either 0 or 1 to denote false and true if conditions are met
				System.out.println("Enter the word to be searched for");
				Scanner file = new Scanner(new File("/home/t/taapkenl/cs111S2017/cs111s2017-dreistadt-taapken/Signers/DOISigners.txt")); //Make sure to link to your own location of DOISigners.txt in your repository
				//Locates and reads through file
				word = input.next(); //Scans for input
				while(file.hasNextLine()){ 

					String line = file.nextLine(); //User input


					if(line.indexOf(word) != -1) //Indexing the words in the file
					{
						System.out.println("Word EXISTS in the file"); //Output if word exists in txt file
						val = 1; 
						break;
					}
					else
					{
						val = 0;
						continue;
					}
				}		            		        
				if(val == 0)
				{
					System.out.println("Word does not exist"); //Output if word does not exits in txt file
				}
				break; //Ends Search feature of program

			}

			while(command.equals("Count")) { //Counts legnth of user input word in txt file
				int val=0;
				System.out.println("Enter the word you want to know the length of ");
				Scanner file = new Scanner(new File("/home/t/taapkenl/cs111S2017/cs111s2017-dreistadt-taapken/Signers/DOISigners.txt")); //Make sure to link to your own location of DOISigners.txt in your repository

				//Locates and reads through file
				word = input.next(); //Scans for input;
				while(file.hasNextLine()){
					String line = file.nextLine();
					if(line.indexOf(word) != -1) //Indexing the words in the file
					{
						val = val + 1;
						System.out.println(val);

						continue;
					}
					else
					{

						continue;
					}
				}		            		        
				break; //Ends running of Count function in program

			}
			while(command.equals("Length")){ //counts number of characters in a line
				System.out.println("Enter the word you want to have counted ");
				Scanner file = new Scanner(new File("/home/t/taapkenl/cs111S2017/cs111s2017-dreistadt-taapken/Signers/DOISigners.txt")); //Make sure to link to your own location of DOISigners.txt in your repository

				//Locates and reads through file
				while(file.hasNextLine()){
					String line = file.nextLine();
					int k = line.length();
					System.out.println(line+": "+k);
				}
				break;

			}
			while(command.equals("Game")){ //Makes a game that prompts user to guess how many letters are in a specific line in the txt file
				int val=0;
				int i;
				System.out.println("Play with a friend!!");
				Scanner file2 = new Scanner(new File("/home/t/taapkenl/cs111S2017/cs111s2017-dreistadt-taapken/Signers/DOISigners.txt")); //Make sure to link to your own location of DOISigners.txt in your repository

				//Locates and reads through file
				for(i=0;i<=l;i++)
				{
					String line = file2.nextLine();
					if(line.indexOf(word) != -1) //Indexing the words in the file
					{
						val = val +1;
						boolean T = (val == l);	
						if (T){System.out.println(val+" "+line); break;}
						else{continue;}
					}
				}	
				length Tree = new length();
				Tree.question(l);
				int p = input.nextInt();
				System.out.println("Have a friend check your work!");
				break; //Ends Game function of program

			}			
			while(command.equals("Scramble")){ //Mutates every line of the txt file
				Scanner file = new Scanner(new File("/home/t/taapkenl/cs111S2017/cs111s2017-dreistadt-taapken/Signers/DOISigners.txt")); //Make sure to link to your own location of DOISigners.txt in your repository

				//Locates and reads through file
				while(file.hasNextLine()){ //Ensures this function goes through each line of code
					String line = file.nextLine(); 
					mutation Mutation = new mutation(); //Actively mutates each line of code
					System.out.println(Mutation.MUTATE(line));						
				}
				break; //Ends Scramble function of program

			}

		}   				
	}
}