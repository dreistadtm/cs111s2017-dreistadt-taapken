//******************************************************************
// Honor Code: The work we are submitting is a result of our own thinking and efforts.
// Meredith Dreistadt & Lisa Taapken
// CMPSC 111 Spring 2017
// Final Project
// Date: May 1, 2017
//
// Purpose: To learn as much as possible about the attached txt file (The Signers of the Declaration of Independence).
//******************************************************************   
public class mutation{
	public String MUTATE(String k){
		int LENGTH = k.length();
		//Declaring all mutations
		String mutation1;
		String mutation2;
		String mutation3;
		String mutation4;
		String mutation5;
		String mutation6;
		String mutation7;
		String mutation8;
		String mutation9;
		String mutation10;
		String mutation11;
		String mutation12;
		String mutation13;
		String mutation14;
		String mutation15;
		String mutation16;
		String mutation17;
		String mutation18;
		String mutation19;
		String mutation20;
		String mutation21;
		String mutation22;
		String mutation23;
		String mutation24;
		String mutation25;
		String mutation26;
		String mutation27;
		String mutation28;
		String mutation29;

		//Telling each line to mutate
		mutation1 = k.substring(0,LENGTH);
		mutation2 = mutation1.toUpperCase();
		mutation3 = mutation2.replace('A','c');
		mutation4 = mutation3.replace('B','d');
		mutation5 = mutation4.replace('C','e');
		mutation6 = mutation5.replace('D','f');
		mutation7 = mutation6.replace('E','g');
		mutation8 = mutation7.replace('F','h');
		mutation9 = mutation8.replace('G','i');
		mutation10 = mutation9.replace('H','j');
		mutation11 = mutation10.replace('I','k');
		mutation12 = mutation11.replace('J','l');
		mutation13 = mutation12.replace('K','m');
		mutation14 = mutation13.replace('L','n');
		mutation15 = mutation14.replace('M','o');
		mutation16 = mutation15.replace('N','p');
		mutation17 = mutation16.replace('O','q');
		mutation18 = mutation17.replace('P','r');
		mutation19 = mutation18.replace('Q','s');
		mutation20 = mutation19.replace('R','t');
		mutation21 = mutation20.replace('S','u');
		mutation22 = mutation21.replace('T','v');
		mutation23 = mutation22.replace('U','w');
		mutation24 = mutation23.replace('V','x');
		mutation25 = mutation24.replace('W','y');
		mutation26 = mutation25.replace('X','z');
		mutation27 = mutation26.replace('Y','a');
		mutation28 = mutation27.replace('Z','b');
		mutation29 = mutation28.toUpperCase();
		return mutation29; //Returning final result of all mutations to user
	}}